/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {
    StyleSheet,
    Image,
    TouchableWithoutFeedback,
    View,
    Animated
} from 'react-native'
import colors from '../defines/colors'

type Props = { onPress: () => void }
export default class MainScreen extends Component<Props> {
    state = {
        pulseAnimation: new Animated.Value(0)
    }
    onPress() {
        this.props.onPress()
    }
    componentDidMount() {
        this.playPulseAnimation()
    }
    playPulseAnimation() {
        this.state.pulseAnimation.setValue(0)
        Animated.sequence([
            Animated.timing(
                this.state.pulseAnimation,
                {
                    toValue: 1,
                    duration: 800
                }
            ),
            Animated.timing(
                this.state.pulseAnimation,
                {
                    toValue: 0,
                    duration: 0
                }
            ),
            Animated.timing(
                this.state.pulseAnimation,
                {
                    toValue: 1,
                    duration: 800
                }
            )
        ]).start(() => {
            setTimeout(() => this.playPulseAnimation(), 3000)
        })
    }
    renderPulse() {
        const scale = this.state.pulseAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 3]
        })
        const fade = this.state.pulseAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [0.3, 0]
        })
        return (<Animated.View style = { [styles.pulse, { opacity: fade, transform: [{ scale }] }]} ></Animated.View>)
    }
    render() {
        return (
            <View style={styles.component}>
                {this.renderPulse()}
                <TouchableWithoutFeedback onPress={() => this.onPress()}>
                    <View style={styles.imageContainer}>
                        <Image source={require('../../resources/images/bubble_pin.png')}></Image>
                    </View>
                </TouchableWithoutFeedback>
            </View>)
    }
}

const styles = StyleSheet.create({
    component: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 185,
        height: 185
    },
    pulse: {
       position: 'absolute',
       width: 70,
       height: 70,
       borderRadius: 40,
       backgroundColor: colors.civiRed
    },
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 85,
        height: 85,
        shadowColor: colors.civiRed,
        shadowRadius: 10,
        shadowOpacity: 0.5,
        shadowOffset: { width: 0, height: 0 },
        elevation: 1
    }
})
