/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {
    StyleSheet,
    Image,
    TouchableWithoutFeedback,
    View
} from 'react-native'

type Props = { menuItemIndex: number, onPress: (idx: number) => void }
export default class MainScreen extends Component<Props> {
    constructor(props) {
        super(props)
    }
    onPress() {
        this.props.onPress(this.props.menuItemIndex)
    }
    getImage(): any {
        switch (this.props.menuItemIndex) {
            case 2:
                return require('../../resources/images/mi2.png')
            case 3:
                return require('../../resources/images/mi3.png')
            case 4:
                return require('../../resources/images/mi4.png')
            case 5:
                return require('../../resources/images/mi5.png')
            case 6:
                return require('../../resources/images/mi6.png')
            case 7:
                return require('../../resources/images/mi7.png')
            case 8:
                return require('../../resources/images/mi8.png')
            case 9:
                return require('../../resources/images/mi9.png')
            default:
                return require('../../resources/images/mi1.png')
        }
    }
    render() {
        return (
            <View>
                <TouchableWithoutFeedback onPress={() => this.onPress()}>
                    <View style={styles.component} >
                        <Image source={this.getImage()}></Image>
                    </View>
                </TouchableWithoutFeedback>
            </View>)
    }
}

const styles = StyleSheet.create({
    component: {
        justifyContent: 'center',
        alignItems: 'center'
    }
})
