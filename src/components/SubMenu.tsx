import React, { Component } from 'react'
import {
    StyleSheet,
    Image,
    TouchableWithoutFeedback,
    View,
    Button,
    Animated
} from 'react-native'
import commonStyles from '../style/common'
import colors from '../defines/colors'

type Props = { category: number, onPress: (category: number, item: number) => void, onBackPress: () => void }
export default class SubMenu extends Component<Props> {
    state = {
        openSubMenuAnimation: new Animated.Value(0)
    }
    constructor(props) {
        super(props)
    }
    onPress(key: number) {
        this.props.onPress(this.props.category, key)
    }
    getImage(): any {
        switch (this.props.category) {
            case 2:
                return require('../../resources/images/mi2.png')
            case 3:
                return require('../../resources/images/mi3.png')
            case 4:
                return require('../../resources/images/mi4.png')
            case 5:
                return require('../../resources/images/mi5.png')
            case 6:
                return require('../../resources/images/mi6.png')
            case 7:
                return require('../../resources/images/mi7.png')
            case 8:
                return require('../../resources/images/mi8.png')
            case 9:
                return require('../../resources/images/mi9.png')
            default:
                return require('../../resources/images/mi1.png')
        }
    }
    componentDidMount() {
        this.state.openSubMenuAnimation.setValue(0)
        Animated.timing(
            this.state.openSubMenuAnimation,
            {
                toValue: 1,
                duration: 500
            }
        ).start()
    }
    renderSubMenuItem = (key: number) => {
        const fadeIn = this.state.openSubMenuAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1]
        })
        return (
            <Animated.View key={key} style={[styles.menuItem, { opacity: fadeIn }]}>
                <TouchableWithoutFeedback onPress={() => this.onPress(key)}>
                    <View>
                        <Image source={this.getImage()}></Image>
                    </View>
                </TouchableWithoutFeedback>
            </Animated.View>)
    }
    renderSubMenuItems = () => {
        let menuItems = []
        for (let i = 0; i < 3; ++i) {
            menuItems.push(this.renderSubMenuItem(i))
        }
        return menuItems
    }
    render() {
        return (
            <TouchableWithoutFeedback>
                <View style={[commonStyles.absolute, commonStyles.container]}>
                    <Button title='Go Back' onPress={() => this.props.onBackPress()}></Button>
                    <View style={[styles.container]} >
                        { this.renderSubMenuItems() }
                    </View>
                </View>
            </TouchableWithoutFeedback>)
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.civiWhite,
        borderRadius: 20,
        height: 150,
        width: '100%',
        margin: 20,
        shadowColor: colors.civiBlack,
        shadowRadius: 10,
        shadowOpacity: 0.3,
        shadowOffset: { width: 0, height: 0 },
        elevation: 10
    },
    menuItem: {
        margin: 15,
        shadowColor: colors.civiBlack,
        shadowRadius: 10,
        shadowOpacity: 0.5,
        shadowOffset: { width: 0, height: 0 },
        elevation: 15
    }
})