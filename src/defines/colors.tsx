const colors = {
    civiRed: '#EB193B',
    civiBlack: 'black',
    civiRedWarm: '#F35250',
    civiWhite: 'white',
    civiLightGray: '#F5FCFF'
}
export default colors