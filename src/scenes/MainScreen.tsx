/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Alert
} from 'react-native'
import MainMenu from './MainMenu'

import MenuButton from '../components/MenuButton'

import text from '../style/text'
import color from '../style/color'
import colors from '../defines/colors'
import commonStyles from '../style/common'

import ItemsHelper from '../utils/ItemsHelper'

type Props = {}
export default class MainScreen extends Component<Props> {
  state = {
      menuOpen: false
  }
  onOpenMenuButtonPress() {
    this.setState({ menuOpen: true })
  }
  onChoice(category, item) {
    const message = `You choose the ${ItemsHelper.itemToName(item)} ${ItemsHelper.categoryToName(category)}!`
    Alert.alert(
      'Gotcha',
      message,
      [
        { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' }
      ])
    this.setState({ menuOpen: false })
  }
  render() {
    return (
      <View style={[commonStyles.container, styles.container]}>
        <View style={[styles.bottomBar, color.bottomBar]} />
        <View style={styles.titleContainer}>
          <Text style={[text.title, color.title, styles.title]}>
            Civi Demo App
          </Text>
          <Text style={[text.subTitle, color.subTitle, styles.subTitle]}>
            Tap on the menu button bellow.
          </Text>
        </View>
        {!this.state.menuOpen && <View style={styles.menuButtonContainer}>
          <MenuButton onPress={() => this.onOpenMenuButtonPress()} />
        </View> }
        {this.state.menuOpen && <View style={[commonStyles.absolute]}>
          <MainMenu onClose={() => this.setState({ menuOpen: false })} onChoice={ (c, i) => this.onChoice(c, i) } />
        </View>}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.civiLightGray,
    overflow: 'visible'
  },
  titleContainer: {
    position: 'absolute',
    top: '30%'
  },
  menuButtonContainer: {
    position: 'absolute',
    bottom: -50,
    margin: 10,
    overflow: 'visible'
  },
  bottomBar: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 50,
    shadowColor: colors.civiBlack,
    shadowRadius: 5,
    elevation: 0
  },
  title: {
     textAlign: 'center',
     margin: 20
  },
  subTitle: {
    textAlign: 'center'
  }
})
