import React, { Component } from 'react'
import TimerMixin from 'react-timer-mixin'
import {
    Animated,
    View,
    Image,
    findNodeHandle,
    ImageProperties,
    StyleSheet,
    LayoutAnimation,
    TouchableWithoutFeedback,
    Platform,
    UIManager
} from 'react-native'
import { BlurView } from 'react-native-blur'
import commonStyles from '../style/common'
import MenuItem from '../components/MenuItem'
import SubMenu from '../components/SubMenu'
import colors from '../defines/colors'

const menuItemSize = 85
const menuItemMargin = 15

type Props = { onClose: () => void, onChoice: (category: number, item: number) => void }
export default class Menu extends Component<Props> {
    backgroundImage: Component<ImageProperties>
    state = {
        // tslint:disable-next-line:no-null-keyword
        viewRef: null,
        menuItems: [],
        menuOpen: false,
        subMenuOpen: false,
        selectedKey: -1,
        openSubMenuAnimation: new Animated.Value(0),
        openItemsCount: 0
    }
    mixins: [TimerMixin]
    constructor(props) {
        super(props)
        if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
            UIManager.setLayoutAnimationEnabledExperimental(true)
        }
    }
    imageLoaded() {
        this.setState({ viewRef: findNodeHandle(this.backgroundImage) })
    }
    componentDidMount() {
        this.openMenu()
    }
    openMenu() {
        this.setState({ menuOpen: true })
    }
    closeMenu() {
        this.setState({ menuOpen: false })
    }
    componentDidUpdate(_prevProps, prevState, _prevContext) {
        if (prevState.menuOpen && !this.state.menuOpen) {
            this.playCloseMenuAnimation(() => {
                this.props.onClose()
            })
        }
        if (!prevState.menuOpen && this.state.menuOpen) {
            this.playOpenMenuAnimation()
        }
        if (prevState.subMenuOpen && !this.state.subMenuOpen) {
            this.closeSubMenu()
        }
        if (prevState.selectedKey === -1 && this.state.selectedKey !== -1) {
            this.openSubMenu()
        }
    }
    playOpenMenuAnimation(onComplete: () => void = undefined) {
        this.renderMenuItems = () => {
            let menuItems = []
            let { openItemsCount } = this.state
            for (let i = 0; i < 9; ++i) {
                menuItems.push(this.renderMenuItem(i, i < openItemsCount))
            }
            return menuItems
        }
        for (let i = 0; i < 10; ++i) {
            if (i < 9) {
                setTimeout(() => {
                    requestAnimationFrame(() => {
                    LayoutAnimation.configureNext({
                        ...LayoutAnimation.Presets.spring, update:
                            {
                                ...LayoutAnimation.Presets.spring.update,
                                springDamping: 0.7,
                                duration: 500
                            }
                    })
                    this.setState({ openItemsCount: i + 1 }) })
                }, i * 80)
            } else {
                if (onComplete) {
                    setTimeout(() => { onComplete() }, i * 80 + 100)
                }
            }
        }
        this.setState({ openItemsCount: 0 })
    }
    playCloseMenuAnimation(onComplete: () => void = undefined) {
        this.renderMenuItems = () => {
            let menuItems = []
            let { openItemsCount } = this.state
            for (let i = 0; i < 9; ++i) {
                menuItems.push(this.renderMenuItem(i, i < openItemsCount))
            }
            return menuItems
        }
        for (let i = 0; i < 10; ++i) {
            if (i < 9) {
                setTimeout(() => {
                    requestAnimationFrame(() => {
                        LayoutAnimation.configureNext({
                            ...LayoutAnimation.Presets.spring, update:
                                {
                                    ...LayoutAnimation.Presets.spring.update,
                                    springDamping: 0.7,
                                    duration: 500
                                }
                        })
                        this.setState({ openItemsCount: 9 - i - 1 })
                    })
                }, i * 80)
            } else {
                if (onComplete) {
                    setTimeout(() => { onComplete() }, i * 80 + 100)
                }
            }
        }
        this.setState({ openItemsCount: 9 })
    }
    renderMenuItem(key: number, isOpen: boolean) {
        return (<View key={key} style={[styles.menuItem, {
            position: isOpen ? 'relative' : 'absolute',
            margin: isOpen ? menuItemMargin : 0,
            shadowColor: colors.civiBlack,
            shadowRadius: 5,
            elevation: isOpen ? 4 : 0,
            shadowOpacity: isOpen ? 0.5 : 0,
            transform: [{ scale: isOpen ? 1 : 0.7 } ]
        }]}>
            <MenuItem onPress={(miIdx) => { this.menuItemPress(miIdx) }} menuItemIndex={(key + 1)}></MenuItem>
        </View>)
    }
    renderOpenCloseSubMenuAnimatedMenuItem(key: number, selectedKey: number) {
        const fade = this.state.openSubMenuAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0]
        })
        const selRow = Math.floor(selectedKey / 3)
        const selCol = selectedKey % 3
        const row = Math.floor(key / 3)
        const col = key % 3
        const angle = Math.atan2(row - selRow, col - selCol)
        const translateX = this.state.openSubMenuAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, Math.abs(col - selCol) * menuItemMargin * 3 * Math.cos(angle)]
        })
        const translateY = this.state.openSubMenuAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [0, Math.abs(row - selRow) * menuItemMargin * 3 * Math.sin(angle)]
        })
        return (<Animated.View key={key} style={[styles.menuItem, {
            position: 'relative',
            shadowColor: colors.civiBlack,
            margin: menuItemMargin,
            shadowRadius: 5,
            elevation: 4,
            shadowOpacity: 0.5,
            opacity: fade,
            transform: [
                { translateX: col > selCol ? translateX : selCol > col ? translateX : 0 },
                { translateY: row > selRow ? translateY : selRow > row ? translateY : 0 }
            ]
        }]}>
            <MenuItem onPress={(miIdx) => { this.menuItemPress(miIdx) }} menuItemIndex={(key + 1)}></MenuItem>
        </Animated.View>)
    }
    menuItemPress(key: number) {
        this.setState({ selectedKey: key })
    }
    openSubMenu() {
        let key = this.state.selectedKey
        this.state.openSubMenuAnimation.setValue(0)
        Animated.timing(
            this.state.openSubMenuAnimation,
            {
                toValue: 1,
                duration: 300
            }
        ).start(() => this.setState({ subMenuOpen: true }))
        this.renderMenuItems = () => {
            let menuItems = []
            for (let i = 0; i < 9; ++i) {
                menuItems.push(this.renderOpenCloseSubMenuAnimatedMenuItem(i, key - 1))
            }
            return menuItems
        }
        this.forceUpdate()
    }
    closeSubMenu() {
        const key = this.state.selectedKey
        this.state.openSubMenuAnimation.setValue(1)
        Animated.timing(
            this.state.openSubMenuAnimation,
            {
                toValue: 0,
                duration: 500
            }
        ).start(() => this.setState({ selectedKey: -1 }))
        this.renderMenuItems = () => {
            let menuItems = []
            for (let i = 0; i < 9; ++i) {
                menuItems.push(this.renderOpenCloseSubMenuAnimatedMenuItem(i, key - 1))
            }
            return menuItems
        }
        this.forceUpdate()
    }
    renderMenuItems = () => []
    renderCloseButton = () => {
        return (<Animated.View style={[commonStyles.container, styles.closeButton]}
            >
            <TouchableWithoutFeedback onPress={() => this.closeMenu()}>
                    <Image source={require('../../resources/images/bubble_close.png')}></Image>
                </TouchableWithoutFeedback>
        </Animated.View>)
    }
    render() {
        return (
            <View style={[commonStyles.container, commonStyles.absolute]}>
                <Image
                    ref={ (img) => { this.backgroundImage = img }}
                    source={ {} }
                    style={commonStyles.absolute}
                    onLoadEnd={this.imageLoaded.bind(this)}
                />
                {Platform.OS === 'ios' && <BlurView
                    style={commonStyles.absolute}
                    blurType='light'
                    blurAmount={10} />}
                {Platform.OS === 'android' &&
                    <View style={[commonStyles.absolute, {backgroundColor: colors.civiWhite, opacity: 0.9}]}></View>}
                <View style={[styles.menu]}>
                    { this.renderMenuItems() }
                    { !this.state.subMenuOpen && this.renderCloseButton() }
                    { this.state.subMenuOpen &&
                    <SubMenu category={this.state.selectedKey}
                        onPress={ (c, i) => this.props.onChoice(c, i) }
                        onBackPress={() => this.setState({ subMenuOpen: false })}>
                    </SubMenu>}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    menu: {
        width: 345,
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
        paddingTop: 90,
        alignItems: 'stretch',
        justifyContent: 'center'
    },
    menuItem: {
        position: 'absolute',
        bottom: 0,
        width: menuItemSize,
        height: menuItemSize,
        elevation: 7
    },
    closeButton: {
        position: 'absolute',
        bottom: 0,
        marginBottom: 10,
        width: menuItemSize,
        height: menuItemSize
    }
})