import { StyleSheet } from 'react-native'

const text = StyleSheet.create({
    title: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 40
    },
    subTitle: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 14
    },
    paragraph: {
        color: 'black',
        fontSize: 14
    }
})

export default text