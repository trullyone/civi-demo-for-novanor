import { StyleSheet } from 'react-native'

const common = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    absolute: {
        position: 'absolute',
        top: 0, left: 0, bottom: 0, right: 0
    }
})

export default common