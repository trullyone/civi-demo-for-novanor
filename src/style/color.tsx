import colors from '../defines/colors'

const color = {
    title: {
        color: colors.civiBlack
    },
    subTitle: {
        color: colors.civiRed
    },
    bottomBar: {
        backgroundColor: colors.civiWhite
    }
}

export default color