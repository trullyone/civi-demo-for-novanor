import { AppRegistry } from 'react-native'
import App from './scenes/MainScreen'

AppRegistry.registerComponent('civi_animations_demo', () => App)
