export default class ItemsHelper {
    static categoryToName(category: number) {
        switch (category) {
            case 1: return 'green circle'
            case 2: return 'purple x'
            case 3: return 'red minus'
            case 4: return 'blue circle'
            case 5: return 'blue row'
            case 6: return 'orange x'
            case 7: return 'yellow O'
            case 8: return 'green dot'
            case 9: return 'pink o'
            default: return ''
        }
    }
    static itemToName(item: number) {
        switch (item) {
            case 0: return 'first'
            case 1: return 'second'
            case 2: return 'third'
            default: return ''
        }
    }
}